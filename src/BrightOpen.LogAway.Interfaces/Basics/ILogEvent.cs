﻿namespace BrightOpen.LogAway.Basics
{
    /// <summary>
    /// Log event knows how to save its content to log evidence
    /// </summary>
	public interface ILogEvent
	{
        /// <summary>
        /// Save event content to log evidence
        /// </summary>
        /// <param name="logEvidence">should not be null</param>
		void SaveTo (ILogEvidence logEvidence);
	}
}

