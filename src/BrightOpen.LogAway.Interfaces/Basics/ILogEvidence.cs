namespace BrightOpen.LogAway.Basics
{
    /// <summary>
    /// Log evidence receives and consolidates evidence
    /// </summary>
	public interface ILogEvidence
	{
        /// <summary>
        /// Add log evidence
        /// </summary>
        /// <param name="data">evidence</param>
		void Add (params object[] data);
	}
}

