﻿using BrightOpen.LogAway.Basics;

namespace BrightOpen.LogAway
{
    /// <summary>
    /// Log abstraction where all implementations and dependency expectations converge
    /// </summary>
    public interface ILog
    {
        /// <summary>
        /// Write a logging event
        /// </summary>
        /// <remarks>
        /// This is the handover point between client code (event producing) and receving code (event processing)
        /// </remarks>
        /// <param name="logEvent">the logging event should not be null</param>
		void Write(ILogEvent logEvent);
    }
}

