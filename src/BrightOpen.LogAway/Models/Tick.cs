﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BrightOpen.LogAway.Models
{
    public class Tick
    {
        public long Frequency { get; }
        public long Stamp { get; }
        public long Reference { get; }
        public long Ticks => Stamp - Reference;
        public double Duration => ((double)Stamp - Reference) / Frequency;

        public Tick(long reference, long stamp, long frequency)
        {
            this.Stamp = stamp;
            this.Frequency = frequency;
            this.Reference = reference;
        }

        public override string ToString()
        {
            return $"{(decimal)(float)Duration}s ({Stamp}-{Reference}/{Frequency})";
        }
    }
}
