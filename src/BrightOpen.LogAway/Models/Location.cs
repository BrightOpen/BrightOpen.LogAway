﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BrightOpen.LogAway.Models
{
    public class Location
    {
        public string Name { get; set; }
        public string File { get; set; }
        public int Line { get; set; }

        public override string ToString()
        {
            return new { Name, File, Line }.ToString();
        }
    }
}
