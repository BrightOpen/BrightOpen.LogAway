﻿using System;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using BrightOpen.LogAway.Logs;
using BrightOpen.LogAway.Basics;
using BrightOpen.LogAway.Models;
using BrightOpen.LogAway.Services;

[assembly: InternalsVisibleTo("Test.LogAway")]
[assembly: InternalsVisibleTo("Test.LogAway.Examples")]

namespace BrightOpen.LogAway
{
    public static class LogExtensions
    {
        /// <summary>
        /// Logs caller location at the time of call to Local()
        /// </summary>
        /// <param name="log">target log</param>
        /// <param name="name">caller name is filled in by the compiler if not provided</param>
        /// <param name="line">caller line is filled in by the compiler if not provided</param>
        /// <param name="file">caller file is filled in by the compiler if not provided</param>
        /// <returns>log</returns> 
        public static ILog Local(this ILog log, [CallerMemberName] string name = "", [CallerLineNumber] int line = 0,
            [CallerFilePath] string file = null)
        {
            return log.Evident("Location", new Location()
            {
                Name = name,
                File = file,
                Line = line
            });
        }

        /// <summary>
        /// Logs caller member name at the time of call to Member()
        /// </summary>
        /// <param name="log">target log</param>
        /// <param name="name">caller name is filled in by the compiler if not provided</param>
        /// <returns>log</returns> 
        public static ILog Member(this ILog log, [CallerMemberName] string name = "")
        {
            return log.Evident("Location", "Name", name);
        }

        /// <summary>
        /// Logs caller file at the time of call to File()
        /// </summary>
        /// <param name="log">target log</param>
        /// <param name="file">caller file is filled in by the compiler if not provided</param>
        /// <returns>log</returns> 
        public static ILog File(this ILog log, [CallerFilePath] string file = "")
        {
            return log.Evident("Location", "File", file);
        }

        /// <summary>
        /// Logs caller line number at the time of call to Line()
        /// </summary>
        /// <param name="log">target log</param>
        /// <param name="line">caller line is filled in by the compiler if not provided</param>
        /// <returns>log</returns> 
        public static ILog Line(this ILog log, [CallerLineNumber] int line = 0)
        {
            return log.Evident("Location", "Line", line);
        }

        public static ISectionLog Section(this ILog log, string name)
        {
            return log.Evident("Name", name).Section();
        }

        public static ISectionLog Section(this ILog log)
        {
            return new SectionLog(log);
        }

        public static ILog OrConsole(this ILog log)
        {
            return log ?? new ConsoleLog();
        }

        public static ILog OrErrorConsole(this ILog log)
        {
            return log ?? new ConsoleErrorLog();
        }

        public static ILog Or(this ILog log, params ILog[] logAlternatives)
        {
            return log ?? logAlternatives?.FirstOrDefault(l => l != null) ?? NoLog.Instance;
        }

        public static ILog OnWrite<T>(this ILog log, Func<T> valueCall, params object[] evidence)
        {
            return new OnWriteLog<T>(log, valueCall, evidence);
        }

        public static ILog Timed(this ILog log, INowGenerator nowgen = null)
        {
            return log.OnWrite((nowgen ?? UtcNowGenerator.Instance).Now, "Time");
        }

        public static ILog Measured(this ILog log, ITickGenerator tickgen = null)
        {
            return log.OnWrite((tickgen ?? new TickGenerator()).Tick, "Duration");
        }

        public static ILog Seq(this ILog log, ISequenceGenerator seqgen = null)
        {
            return log.OnWrite((seqgen ?? StaticSequenceGenerator.Instance).Next, "Seq");
        }

        public static ILog Evident(this ILog log, params object[] evidence)
        {
            return new EvidenceLog(log, evidence);
        }

        public static ILog Correlated(this ILog log, string correlationId = null)
        {
            return log.Evident("CorrelationId", correlationId ?? Guid.NewGuid().ToString("N"));
        }

        public static ILog Host(this ILog log)
        {
            return log.Evident("Host", Environment.MachineName);
        }

        public static ILog Pid(this ILog log, IProcessIdProvider pidprov = null)
        {
            return log.Evident("Pid", (pidprov ?? ProcessIdProvider.Instance).GetProcessId());
        }

        public static ILog For<TOwner>(this ILog log, TOwner owner = default(TOwner), IOwnerIdentifier ownident = null)
        {
            return log.Evident("Name", (ownident ?? OwnerIdentifier.Instance).GetOwnerId(owner));
        }

        public static ISectionLog None(this ILog log)
        {
            return NoLog.Instance;
        }

        public static ISectionLog Disposable(this ILog log)
        {
            return new DisposableLog(log, dispose: false);
        }

        public static ILog OnEntry(this ILog log, params object[] evidence)
        {
            return new EntryLog(log, evidence);
        }

        public static ISectionLog OnExit(this ILog log, params object[] evidence)
        {
            return new ExitLog(log, dispose: false, evidence: evidence);
        }

        public static ISectionLog OnExitDispose(this ISectionLog log, params object[] evidence)
        {
            return new ExitLog(log, dispose: true, evidence: evidence);
        }

        public static void Write(this ILog log, params object[] evidence)
        {
            log.Write(new Event().WithEvidence(evidence));
        }

        public static ILogEvent WithEvidence(this ILogEvent logEvent, params object[] evidence)
        {
            return new Event(logEvent, evidence);
        }

        public static ILog Log(this TextWriter writer)
        {
            return new TextLog(writer);
        }
    }
}

