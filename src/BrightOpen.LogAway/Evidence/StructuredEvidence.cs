﻿using System;
using BrightOpen.LogAway.Basics;
using System.Collections;
using System.Collections.Generic;
using System.Text;

namespace BrightOpen.LogAway.Evidence
{
    public sealed class StructuredEvidence : Hashtable, ILogEvidence
    {
        #region ILogEvidence implementation

        public void Add(params object[] data)
        {
            if (data == null || data.Length == 0)
                return;

            IDictionary dict = this;

            // merge a dictionary hierarchy (A,B,C,D) => [A][B]=dict

            for (var i = 0; i < data.Length - 2; i++)
            {
                var key = data[i];
                var newdict = dict.Contains(key)
                    ? dict[key] as MDic ?? new MDic() { { string.Empty, dict[key] } }
                    : new MDic();

                dict[key] = newdict;
                dict = newdict;
            }

            object key2;
            object value;

            // merge dict member (A,B,C,D) => dict[C]=D

            if (data.Length > 1)
            {
                key2 = data[data.Length - 2];
                value = data[data.Length - 1];
            }
            else
            {
                key2 = "Message";
                value = data[data.Length - 1];
            }

            if (dict.Contains(key2))
            {
                var array = dict[key2] as MSeq ?? new[] { dict[key2] } as IEnumerable<object>;
                dict[key2] = new MSeq(array, value);
            }
            else
            {
                dict[key2] = value;
            }
        }

        public override string ToString()
        {
            return DicToString(this);
        }

        private static string DicToString(IDictionary dic)
        {
            if (dic == null)
                return null;

            var sb = new StringBuilder();
            sb.Append("{");
            foreach (DictionaryEntry item in dic)
            {
                if (sb.Length != 1)
                    sb.Append(",");

                sb.AppendFormat("{0}={1}", ValueToString(item.Key), ValueToString(item.Value));
            }
            sb.Append("}");
            return sb.ToString();
        }

        private static string ValueToString(object value)
        {
            return value == null 
                ? "(null)" 
                : (value as DateTime?)?.ToString("O")
                ?? DicToString(value as IDictionary)
                ?? Convert.ToString(value);
        }

        /// <summary>
        /// Using private type to prevent modifying external dictionaries
        /// </summary>
        class MDic : Hashtable
        {
            public override string ToString()
            {
                return DicToString(this);
            }
        }

        /// <summary>
        /// Using private type to prevent assuming too much about enumerables
        /// </summary>
        class MSeq : IEnumerable<object>
        {
            private readonly IEnumerable<object> _items;

            private readonly object _tail;

            public MSeq(IEnumerable<object> items, object tail)
            {
                this._items = items;
                this._tail = tail;
            }

            public override string ToString()
            {
                var sb = new StringBuilder();
                sb.Append("[");
                foreach (var item in this)
                {
                    if (sb.Length != 1)
                        sb.Append(",");

                    sb.Append(ValueToString(item));
                }
                sb.Append("]");
                return sb.ToString();
            }

            /// <summary>
            /// Deferring enumeration until it is needed
            /// </summary>
            IEnumerable<object> Enumerate()
            {
                if (_items != null)
                    foreach (var item in _items)
                        yield return item;

                if (_tail != null)
                    yield return _tail;
            }

            #region IEnumerable implementation

            IEnumerator<object> IEnumerable<object>.GetEnumerator()
            {
                return Enumerate().GetEnumerator();
            }

            #endregion

            #region IEnumerable implementation

            IEnumerator IEnumerable.GetEnumerator()
            {
                return Enumerate().GetEnumerator();
            }

            #endregion
        }

        #endregion
    }
}

