﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BrightOpen.LogAway.Basics;

namespace BrightOpen.LogAway.Evidence
{
    public sealed class ObjectiveEvidence : IEnumerable<object[]>, ILogEvidence
    {
        private readonly List<object[]> _evidence = new List<object[]>();

        public void Add(params object[] data)
        {
            _evidence.Add(data);
        }

        public IEnumerator<object[]> GetEnumerator()
        {
            return _evidence.ToArray().Cast<object[]>().GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        public override string ToString()
        {
            var sb = new StringBuilder();

            for (var i = 0; i < _evidence.Count; i++)
            {
                var evidence = _evidence[i];

                for (var j = 0; j < evidence?.Length; j++)
                {
                    sb.Append(evidence[j]);

                    if (j + 2 < evidence.Length)
                        sb.Append(".");
                    else if (j + 2 == evidence.Length)
                        sb.Append("=");
                }

                if (i + 2 <= _evidence.Count)
                    sb.Append("; ");
            }

            return sb.ToString();
        }
    }
}
