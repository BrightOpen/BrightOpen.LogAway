﻿using System;
using BrightOpen.LogAway.Basics;

namespace BrightOpen.LogAway.Logs
{
    public sealed class ConsoleLog : ILog
    {
        private readonly ILog _log;

        public ConsoleLog() 
        {
            _log = Console.Out.Log();
        }

        public void Write(ILogEvent logEvent)
        {
            _log?.Write(logEvent);
        }
    }
}
