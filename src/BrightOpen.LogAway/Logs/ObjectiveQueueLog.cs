﻿using BrightOpen.LogAway.Basics;
using System.Collections.Generic;
using System.Linq;
using BrightOpen.LogAway.Evidence;
using System;
using System.Collections;

namespace BrightOpen.LogAway.Logs
{
    /// <summary>
    /// Put log event into a queue 
    /// </summary>
    /// <remarks>
    /// This is useful for debugging and testing
    /// </remarks>
	internal sealed class ObjectiveQueueLog : ILog, IEnumerable<object[][]>
    {
	    private readonly Queue<object[][]> _queue;
        
		public ObjectiveQueueLog(Queue<object[][]> queue = null)
		{
			this._queue = queue ?? new Queue<object[][]> ();
		}

        public IEnumerator<object[][]> GetEnumerator()
        {
            return _queue.ToArray().OfType<object[][]>().GetEnumerator();
        }

        #region ILog implementation

        public void Write (ILogEvent logEvent)
		{
            var objevidence = new ObjectiveEvidence();
            logEvent.SaveTo(objevidence);
			_queue.Enqueue (objevidence.ToArray());
		}

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        #endregion
    }
}

