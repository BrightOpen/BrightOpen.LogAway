﻿using BrightOpen.LogAway.Basics;

namespace BrightOpen.LogAway.Logs
{
    internal sealed class EntryLog : ILog
    {
        private readonly ILog _log;

        public EntryLog(ILog log, params object[] evidence)
        {
            this._log = log;

            if (_log == null)
                return;

            if (evidence == null)
                return;

            this._log.Write(new Event().WithEvidence(evidence));
        }

        #region ILog implementation

        public void Write(ILogEvent e)
        {
            this._log?.Write(e);
        }

        #endregion
        
    }
}

