﻿using System;
using BrightOpen.LogAway.Basics;

namespace BrightOpen.LogAway.Logs
{
    internal sealed class SectionLog : ISectionLog
    {
        private readonly ISectionLog _log;

        public SectionLog(ILog log)
        {
            this._log = log
                .OnEntry("Message", "Enter")
                .Measured( )
                .OnExit("Message", "Exit");
        }

        #region ILog implementation

        public void Write(ILogEvent e)
        {
            this._log?.Write(e);
        }

        #endregion

        #region IDisposable implementation

        void IDisposable.Dispose()
        {
            this._log?.Dispose();
        }

        #endregion
    }
}

