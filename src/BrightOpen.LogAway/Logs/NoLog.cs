﻿using BrightOpen.LogAway.Basics;

namespace BrightOpen.LogAway.Logs
{
    public sealed class NoLog : ISectionLog
    {
        public static ISectionLog Instance { get; } = new NoLog();

        public void Write(ILogEvent logEvent)
        {
        }

        public void Dispose()
        {
        }
    }
}
