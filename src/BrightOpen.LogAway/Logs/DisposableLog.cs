﻿using System;
using BrightOpen.LogAway.Basics;

namespace BrightOpen.LogAway.Logs
{
    internal class DisposableLog : ISectionLog
    {
        private readonly ILog _log;
        private readonly bool _dispose;

        public DisposableLog(ILog log, bool dispose)
        {
            _log = log;
            _dispose = dispose;
        }

        void IDisposable.Dispose()
        {
            Dispose(true);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (_dispose)
                (_log as IDisposable)?.Dispose();
        }

        public void Write(ILogEvent logEvent)
        {
            _log?.Write(logEvent);
        }
    }
}