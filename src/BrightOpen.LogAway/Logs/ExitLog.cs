﻿using System.Linq;
using BrightOpen.LogAway.Basics;

namespace BrightOpen.LogAway.Logs
{
    internal sealed class ExitLog : DisposableLog
    {
        private readonly ILog _log;
        private readonly object[] _evidence;

        public ExitLog(ILog log, bool dispose, params object[] evidence) : base(log, dispose)
        {
            this._log = log;
            this._evidence = evidence;
        }

        protected override void Dispose(bool disposing)
        {
            if (_log == null)
                return;

            if (_evidence == null || !_evidence.Any())
                return;
            
            this._log.Write(new Event().WithEvidence(_evidence));

            base.Dispose(disposing);
        }
    }
}

