﻿using System;
using BrightOpen.LogAway.Basics;

namespace BrightOpen.LogAway.Logs
{
    public sealed class ConsoleErrorLog : ILog
    {
        private readonly ILog _log;

        public ConsoleErrorLog()
        {
            _log = Console.Error.Log();
        }

        public void Write(ILogEvent logEvent)
        {
            _log?.Write(logEvent);
        }
    }
}
