﻿using System.IO;
using BrightOpen.LogAway.Basics;

namespace BrightOpen.LogAway.Logs
{
    internal sealed class TextLog : ILog
    {
        private readonly TextWriter _writer;

        public TextLog(TextWriter writer)
        {
            this._writer = writer;
        }

        public void Write(ILogEvent logEvent)
        {
            _writer.WriteLine(logEvent);
            _writer.Flush();
        }
    }
}
