﻿using System;
using System.Linq;
using BrightOpen.LogAway.Basics;

namespace BrightOpen.LogAway.Logs
{
    internal sealed class OnWriteLog<T> : ILog
    {
        private readonly ILog _log;
        private readonly Func<T> _valueCall;
        private readonly object[] _evidence;

        public OnWriteLog(ILog log, Func<T> valueCall, params object[] evidence)
        {
            this._log = log;
            _valueCall = valueCall;
            _evidence = evidence ?? new object[0];
        }

        public void Write(ILogEvent logEvent)
        {
            if (_log == null)
                return;

            logEvent = _valueCall == null
                ? logEvent.WithEvidence(_evidence)
                : logEvent.WithEvidence(_evidence.Concat(new object[] {_valueCall()}).ToArray());

            _log.Write(logEvent);
        }
    }
}