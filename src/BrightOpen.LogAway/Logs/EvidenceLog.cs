﻿using BrightOpen.LogAway.Basics;

namespace BrightOpen.LogAway.Logs
{
    /// <summary>
    /// Adds evidence to each log
    /// </summary>
    internal sealed class EvidenceLog : ILog
    {
        private readonly ILog _log;
        private readonly object[] _evidence;

        public EvidenceLog(ILog log, params object[] evidence)
        {
            this._log = log;
            this._evidence = evidence;
        }

        public void Write(ILogEvent logEvent)
        {
            _log?.Write(logEvent.WithEvidence(_evidence));
        }
    }
}
