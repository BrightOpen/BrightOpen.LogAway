﻿using BrightOpen.LogAway.Evidence;

namespace BrightOpen.LogAway.Basics
{
    public class Event : ILogEvent
    {
        public Event() : this(null)
        {
        }

        public Event(ILogEvent parentLogEvent, object[] evidence = null)
        {
            _parent = parentLogEvent;
            _evidence = evidence;
        }

        private readonly object[] _evidence;
        private readonly ILogEvent _parent;

        #region ILogEvent implementation

        public void SaveTo(ILogEvidence logEvidence)
        {
            if (_parent != null)
                _parent.SaveTo(logEvidence);

            if (_evidence != null)
                logEvidence.Add(_evidence);
        }

        #endregion

        public override string ToString()
        {
            //var evidence = new StructuredEvidence();
            var evidence = new ObjectiveEvidence();
            SaveTo(evidence);
            return evidence.ToString();
        }
    }
}

