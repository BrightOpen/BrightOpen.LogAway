﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace BrightOpen.LogAway.Services
{
    public interface ISequenceGenerator
    {
        long Next();
    }

    public sealed class StaticSequenceGenerator : ISequenceGenerator
    {
        private static long _seq;

        public static ISequenceGenerator Instance { get;  } = new StaticSequenceGenerator();

        public long Next()
        {
            return Interlocked.Increment(ref _seq);
        }
    }
}
