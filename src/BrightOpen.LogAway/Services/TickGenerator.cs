﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using BrightOpen.LogAway.Models;

namespace BrightOpen.LogAway.Services
{
    public interface ITickGenerator
    {
        Tick Tick();
    }

    public sealed class TickGenerator : ITickGenerator
    {
        private readonly long _reference;

        public TickGenerator()
        {
            _reference = Stopwatch.GetTimestamp();
        }

        public static ITickGenerator Instance { get;  } = new TickGenerator();

        public Tick Tick()
        {
            return new Tick(_reference, Stopwatch.GetTimestamp(), Stopwatch.Frequency);
        }
    }
}
