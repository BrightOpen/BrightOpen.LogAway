﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace BrightOpen.LogAway.Services
{
    public interface INowGenerator
    {
        DateTime Now();
    }

    public sealed class UtcNowGenerator : INowGenerator
    {
        public static INowGenerator Instance { get;  } = new UtcNowGenerator();

        public DateTime Now()
        {
            return DateTime.UtcNow;
        }
    }
}
