﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BrightOpen.LogAway.Services
{
    public interface IOwnerIdentifier
    {
        string GetOwnerId<T>(T owner);
    }

    public sealed class OwnerIdentifier : IOwnerIdentifier
    {
        public static IOwnerIdentifier Instance { get; } = new OwnerIdentifier();

        public string GetOwnerId<T>(T owner)
        {
            return owner?.GetType().FullName ?? typeof(T).FullName;
        }
    }
}
