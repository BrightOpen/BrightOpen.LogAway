﻿using System;
using System.Diagnostics;

namespace BrightOpen.LogAway.Services
{
    public interface IProcessIdProvider
    {
        int GetProcessId();
    }

    public sealed class ProcessIdProvider : IProcessIdProvider
    {
        private static readonly int CurrentPid = Process.GetCurrentProcess().Id;

        public static IProcessIdProvider Instance { get; } = new ProcessIdProvider();

        public int GetProcessId()
        {
            return CurrentPid;
        }
    }
}
