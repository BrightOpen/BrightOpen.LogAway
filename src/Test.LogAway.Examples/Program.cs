﻿using System;
using System.Linq;
using System.Linq.Expressions;
using Test.LogAway.Examples.Basics;

namespace Test.LogAway.Examples
{
    public static class Program
    {
        public static int Main(string[] args)
        {
            Show(() => new IntroLogging().MakeTheWorldABetterPlace());

            Show(() => new NormalLogging().Write());
            Show(() => new NormalLogging().WriteNestedEvidence());
            Show(() => new NormalLogging().WriteEvent());
            Show(() => new NormalLogging().WriteEventWithMultipleEvidence());
            Show(() => new NormalLogging().LocalLog());
            Show(() => new NormalLogging().LocalSectionLog());
            Show(() => new ConsoleLogging().ConsoleLog());
            Show(() => new ConsoleLogging().ConsoleErrorLog());
            Show(() => new FunkyLogging().FunkyExtensions());
            Show(() => new FunkyLogging().FunkyAnonyms());
            return 0;
        }

        private static void Show(Expression<Func<object>> action)
        {
            Console.WriteLine(string.Join("", Enumerable.Repeat("-", Console.BufferWidth - 1)));
            Console.WriteLine($"Running {action}");
            Console.WriteLine(action.Compile()());
        }
    }
}
