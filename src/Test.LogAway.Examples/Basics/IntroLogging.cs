﻿namespace Test.LogAway.Examples.Basics
{
    using BrightOpen.LogAway;
    using Xunit;

    public class IntroLogging
    {
        private readonly ILog _log;
        
        public IntroLogging()
        {
            // In your lib/app, you'll probably want log to be injected: (ILog log)
            var log = null as ILog;

            // Here we allow it to be null in which case we default to console: .OrConsole()
            // Then we take a class specific instance of the loger: .For(this)
            // so that the name is available in all log events
            this._log = log.OrConsole().For(this);
        }

        [Fact]
        public IntroLogging MakeTheWorldABetterPlace()
        {
            using (var log = _log.Local().Section())
            {
                // if your code is semantically structured, you can move away from verbose blabber and focus on facts
                // that can be easily processed and analised while maintaining readability for humans:

                log.Write("Done", new { By = "superhero" });
            }

            return this;

            // Message: Enter; Line: 25; Name: MakeTheWorldABetterPlace, Test.LogAway.Examples.Basics.IntroLogging, ; File: W:\rob\BrightOpen.LogAway\src\Test.LogAway.Examples\Basics\IntroLogging.cs;
            // Done: { By = superhero }; Line: 25; Name: MakeTheWorldABetterPlace, Test.LogAway.Examples.Basics.IntroLogging, ; File: W:\rob\BrightOpen.LogAway\src\Test.LogAway.Examples\Basics\IntroLogging.cs;
            // Message: Exit; Line: 25; Name: MakeTheWorldABetterPlace, Test.LogAway.Examples.Basics.IntroLogging, ; File: W:\rob\BrightOpen.LogAway\src\Test.LogAway.Examples\Basics\IntroLogging.cs;

        }
    }
}
