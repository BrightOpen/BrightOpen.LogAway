﻿using System;
using System.IO;
using BrightOpen.LogAway;
using BrightOpen.LogAway.Logs;
using FluentAssertions;
using Xunit;

namespace Test.LogAway.Examples.Basics
{
    public class FunkyLogging : IDisposable
    {
        private readonly StringWriter _sw;

        public FunkyLogging()
        {
            this._sw = new StringWriter();
        }

        public void Dispose()
        {
            _sw.Dispose();
        }

        public override string ToString()
        {
            return _sw.ToString();
        }

        [Fact]
        public FunkyLogging FunkyExtensions()
        {
            // fluent style
            var log =
                (null as ILog)
                .OrConsole()
                .Timed()
                .Seq()
                .Pid()
                .Host()
                .Evident("This","is","that")
                .For(this)
                .Local()
                .Section("wow");

            log.Write("That's funky, too");

            return this;
        }

        [Fact]
        public FunkyLogging FunkyAnonyms()
        {
            var log = new TextLog(_sw);

            log.Write("WeLoveAnonyms", new { Name = "anonymous", Gender = "none of your busines", Mission = "liberation" });

            _sw.ToString()
                .Should().Contain("Love")
                .And.Contain("anonymous")
                .And.Contain("liberation");

            return this;
        }
    }
}
