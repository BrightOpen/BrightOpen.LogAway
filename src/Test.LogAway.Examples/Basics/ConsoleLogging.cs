﻿using BrightOpen.LogAway.Logs;
using BrightOpen.LogAway;
using Xunit;

namespace Test.LogAway.Examples.Basics
{
    public class ConsoleLogging
    {

        [Fact]
        public string ConsoleLog()
        {
            var sut = new ConsoleLog();

            sut.Write("Hi");

            return null;
        }

        [Fact]
        public string ConsoleErrorLog()
        {
            var sut = new ConsoleErrorLog();

            sut.Write("Hi");

            return null;
        }
    }
}
