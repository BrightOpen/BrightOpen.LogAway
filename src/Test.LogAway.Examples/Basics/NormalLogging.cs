﻿using System;
using System.IO;
using BrightOpen.LogAway;
using BrightOpen.LogAway.Basics;
using BrightOpen.LogAway.Logs;
using FluentAssertions;
using Xunit;

namespace Test.LogAway.Examples.Basics
{
    public class NormalLogging : IDisposable
    {
        private readonly StringWriter _sw;
        private readonly TextLog _log;

        public NormalLogging()
        {
            this._sw = new StringWriter();
            this._log = new TextLog(_sw);
        }

        public void Dispose()
        {
            _sw.Dispose();
        }

        public override string ToString()
        {
            return _sw.ToString();
        }

        [Fact]
        public NormalLogging Write()
        {
            var sut = _log as ILog;

            sut.Write("hair", "brown");

            _sw.ToString()
                .Should().Contain("hair")
                .And.Contain("brown");

            return this;
        }

        [Fact]
        public NormalLogging WriteNestedEvidence()
        {
            var sut = _log as ILog;

            sut.Write("fox", "jumps", "far");

            _sw.ToString()
                .Should().Contain("fox")
                .And.Contain("jump")
                .And.Contain("far");

            return this;
        }

        [Fact]
        public NormalLogging WriteEvent()
        {
            var sut = _log as ILog;

            sut.Write(new Event()
                .WithEvidence("hair", "brown")
                .WithEvidence("eyes", "green"));

            _sw.ToString()
                .Should().Contain("eyes")
                .And.Contain("brown");

            return this;
        }

        [Fact]
        public NormalLogging WriteEventWithMultipleEvidence()
        {
            var sut = _log as ILog;

            sut.Write(new Event()
                .WithEvidence("can", "walk")
                .WithEvidence("can", "talk"));

            _sw.ToString()
                .Should().Contain("can")
                .And.Contain("walk")
                .And.Contain("talk");

            return this;
        }



        [Fact]
        public NormalLogging LocalLog()
        {
            _log.Local().Write("hello");

            // Local() captures caller context: member name, file line and file path
            /*
            Message: Enter; Name: LocalLog; Line: 99; File: W:\rob\BrightOpen.LogAway\src\Test.LogAway.Examples\Basics\NormalLogging.cs;
            Message: hello; Name: LocalLog; Line: 99; File: W:\rob\BrightOpen.LogAway\src\Test.LogAway.Examples\Basics\NormalLogging.cs;
            */

            _sw.ToString()
                .Should().Contain("hello")
                .And.Contain(nameof(LocalLog))
                .And.Contain(nameof(NormalLogging))
                .And.Contain("Line");

            return this;
        }

        [Fact]
        public NormalLogging LocalSectionLog()
        {
            using (var log = _log.Local().Section())
            {
                log.Write("hello");
            }

            // Local() generates section entry and exit
            /*
            Message: Enter; Name: LocalSectionLog; Line: 119; File: W:\rob\BrightOpen.LogAway\src\Test.LogAway.Examples\Basics\NormalLogging.cs;
            Message: hello; Name: LocalSectionLog; Line: 119; File: W:\rob\BrightOpen.LogAway\src\Test.LogAway.Examples\Basics\NormalLogging.cs;
            Message: Exit; Name: LocalSectionLog; Line: 119; File: W:\rob\BrightOpen.LogAway\src\Test.LogAway.Examples\Basics\NormalLogging.cs;
            */

            _sw.ToString()
                .Should().Contain("hello")
                .And.Contain("Enter")
                .And.Contain("Exit");

            return this;
        }

    }
}
