using System;
using Microsoft.Extensions.Logging;

namespace BrightOpen.LogAway.DotNetLog
{
    public class Logger : ILogger
    {
        private readonly ILog _log;
        private readonly Provider _provider;

        public Logger(Provider provider, ILog log, string category)
        {
            _log = log.Evident(nameof(category), category);
            _provider = provider;
        }

        public void Log<TState>(LogLevel logLevel, EventId eventId, TState state, Exception exception, Func<TState, Exception, string> formatter)
        {
            _log
                .Evident(nameof(eventId), eventId)
                .Evident(nameof(logLevel), logLevel)
                .Write(formatter(state, exception));
        }

        public bool IsEnabled(LogLevel logLevel)
        {
            return logLevel >= _provider.Level;
        }

        public IDisposable BeginScope<TState>(TState state)
        {
            var log = _log.Evident(nameof(state), state);

            return _provider.Level < LogLevel.Information
                ? log.Section()
                : log.Disposable();
        }
    }
}