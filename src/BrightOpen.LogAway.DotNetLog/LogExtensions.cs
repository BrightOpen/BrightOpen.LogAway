﻿using Microsoft.Extensions.Logging;

namespace BrightOpen.LogAway.DotNetLog
{
    public static class LogExtensions
    {
        public static ILoggerProvider DotNetLogProvider(this ILog log, LogLevel level)
        {
            return new Provider(log, level);
        }
    }
}

