﻿using System;
using Microsoft.Extensions.Logging;

namespace BrightOpen.LogAway.DotNetLog
{
    public class Provider : ILoggerProvider
    {
        private readonly ILog _log;
        public LogLevel Level { get; set; }

        public Provider(ILog log, LogLevel level)
        {
            _log = log;
            Level = level;
        }

        public void Dispose()
        {
            (_log as IDisposable)?.Dispose();
        }

        public ILogger CreateLogger(string categoryName)
        {
            return new Logger(this, _log.OrConsole(), categoryName);
        }
    }
}
