﻿using System;
using BrightOpen.LogAway;
using BrightOpen.LogAway.Basics;
using BrightOpen.LogAway.Logs;
using NSubstitute;
using Xunit;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using FluentAssertions;

namespace Test.LogAway.Logs
{
    public class DisposableLogTests
    {
        [Fact]
        public void ExtensionMethod_NotNull_WhenLogNull()
        {
            var sut = (null as ILog).Disposable();

            sut.Should().NotBeNull();
        }

        [Fact]
        public void ExtensionMethod_IsDisposableLog_WhenLogNull()
        {
            var sut = (null as ILog).Disposable();

            sut.Should().BeAssignableTo<ISectionLog>();
        }

        [Fact]
        public void ExtensionMethod_Write_DoesntFail_WhenLogNull()
        {
            var sut = (null as ILog).Disposable();

            sut.Write("Hi");
        }

        [Fact]
        public void ExtensionMethod_WrapsLog()
        {
            var logmock = Substitute.For<ISectionLog>();

            var sut = logmock.Disposable();

            sut.Should().NotBe(logmock);
        }

        [Fact]
        public void Write_DoesntFail_WhenLogNull()
        {
            var sut = new DisposableLog(null, false);

            sut.Write("Hi");
        }

        [Fact]
        public void Dispose_DoesntFail_WhenLogNull()
        {
            var sut = new DisposableLog(null, false) as ISectionLog;

            sut.Dispose();
        }

        [Fact]
        public void Write_PassesThrough()
        {
            var e = new Event();
            var logmock = Substitute.For<ISectionLog>();

            var sut = new DisposableLog(logmock, false) as ISectionLog;

            sut.Write(e);

            logmock.Received(1).Write(e);
        }

        [Fact]
        public void Dispose_IsWithheld()
        {
            var logmock = Substitute.For<ISectionLog>();

            var sut = new DisposableLog(logmock, false) as ISectionLog;

            sut.Dispose();

            logmock.Received(0).Dispose();
        }
    }
}

