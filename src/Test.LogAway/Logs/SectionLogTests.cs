﻿using System;
using BrightOpen.LogAway;
using BrightOpen.LogAway.Basics;
using BrightOpen.LogAway.Logs;
using NSubstitute;
using Xunit;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using BrightOpen.LogAway.Models;
using FluentAssertions;

namespace Test.LogAway.Logs
{
    public class SectionLogTests
    {
        [Fact]
        public void CtorAndDispose_WritesEntryAndExit()
        {
            var log = Substitute.For<ILog>();
            using (new SectionLog(log))
            {
            }
            log.ReceivedWithAnyArgs(2).Write(new Event());
        }

        [Fact]
        public void Section_WritesEntryAndExit()
        {
            var log = Substitute.For<ILog>();
            using (log.Section())
            {
            }
            log.ReceivedWithAnyArgs(2).Write(new Event());
        }

        [Fact]
        public void Local_CapturesCallerLocation()
        {
            var q = new ObjectiveQueueLog();

            var sut = q.Local();

            sut.Write(new Event());

            q.Single()
                .SelectMany(evidence => evidence)
                .OfType<Location>()
                .Single()
                .Name
                .ShouldBeEquivalentTo(nameof(Local_CapturesCallerLocation));
        }

        [Fact]
        public void Section_WritesName()
        {
            var q = new ObjectiveQueueLog();
            using (var sut = q.Section("named"))
            {
                sut.Write(new Event());
            }

            var names = q.Select(evt => evt
                .Where(evidence => "Name".Equals(evidence.FirstOrDefault()))
                .Select(evidence => evidence.LastOrDefault())
                .ToArray());

            names.ShouldBeEquivalentTo(new[]
            {
                new[] {"named"},
                new[] {"named"},
                new[] {"named"},
            });
        }

        [Fact]
        public void For_WritesOwnerTypeName()
        {
            var q = new ObjectiveQueueLog();

            var sut = q.For(this);

            sut.Write(new Event());

            var locations = q.SelectMany(entry => entry).SelectMany(evidence => evidence).OfType<string>().ToList();

            locations.Should().Contain("Test.LogAway.Logs.SectionLogTests");
        }

        [Fact]
        public void ForSection_WritesNestedNames()
        {
            var q = new ObjectiveQueueLog();

            using (var sut = q.For(this).Section())
            {
                using (var sut2 = sut.For(this).Section())
                {
                    sut2.Write(new Event());
                }
            }

            var names = q.Skip(2).FirstOrDefault()
                .Where(evidence => "Name".Equals(evidence.FirstOrDefault()))
                .Select(evidence => evidence.LastOrDefault())
                .ToArray();

            names.ShouldBeEquivalentTo(new[] { "Test.LogAway.Logs.SectionLogTests", "Test.LogAway.Logs.SectionLogTests" });
        }

    }
}

