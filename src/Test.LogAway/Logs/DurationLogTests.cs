﻿using System;
using BrightOpen.LogAway;
using BrightOpen.LogAway.Basics;
using BrightOpen.LogAway.Logs;
using NSubstitute;
using Xunit;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BrightOpen.LogAway.Models;
using FluentAssertions;

namespace Test.LogAway.Logs
{
    public class DurationLogTests
    {
        [Fact]
        public void ExtensionMethod_NotNull_WhenLogNull()
        {
            var sut = (null as ILog).Measured();

            sut.Should().NotBeNull();
        }

        [Fact]
        public void Write_DoesntFail_WhenLogNull()
        {
            var sut = (null as ILog).Measured();

            sut.Write("Hi");
        }

        [Fact]
        public void WrapsLog()
        {
            var logmock = Substitute.For<ISectionLog>();

            var sut = logmock.Measured();

            sut.Should().NotBe(logmock);
        }

        [Fact]
        public void Write_PassesThrough()
        {
            var e = new Event();
            var logmock = Substitute.For<ISectionLog>();

            var sut = logmock.Measured();

            sut.Write(e);

            logmock.ReceivedWithAnyArgs(1).Write(e);
        }

        [Fact]
        public async Task Write_MeasuresDuration()
        {
            var e = new Event();
            var q = new ObjectiveQueueLog();

            var sut = q.Measured();

            await Task.Delay(TimeSpan.FromMilliseconds(100));

            sut.Write(e);

            q.Single().Last().Last()
                .Should().BeAssignableTo<Tick>()
                .Which
                .Duration.Should()
                .BeGreaterThan(0.1);
        }
    }
}

