﻿using Xunit;
using System;
using System.Reflection;
using BrightOpen.LogAway;
using BrightOpen.LogAway.Basics;

namespace Test.LogAway
{
	public class InterfaceExpectations
	{
		[Theory,
			InlineData ("BrightOpen.LogAway.Interfaces", "BrightOpen.LogAway.ILog"),
			InlineData ("BrightOpen.LogAway.Interfaces", "BrightOpen.LogAway.Basics.ILogEvent"),
			InlineData ("BrightOpen.LogAway.Interfaces", "BrightOpen.LogAway.Basics.ILogEvidence"),
		]
		public void Interface_IsPresent_WithCorrectName (string asmname, string typename)
		{
            var name = new AssemblyName(asmname);
			Assert.NotNull (Assembly.Load (name).GetType (typename));
		}

		[Theory, 
			InlineData (typeof(ILog)),
			InlineData (typeof(ILogEvent)),
		]
		public void Interface_HasJustOneMember (Type type)
		{
			Assert.Equal (1, type.GetMembers ().Length);
		}

		[Theory,
			InlineData (typeof(ILog), "Write"),
			InlineData (typeof(ILogEvent), "SaveTo"),
			InlineData (typeof(ILogEvidence), "Add"),
		]
		public void Interface_HasMethod (Type type, string methodname)
		{
			Assert.NotNull (type.GetMethod (methodname));
		}

		[Theory,
			InlineData (typeof(ILog), "Write", "logEvent", typeof(ILogEvent)),
			InlineData (typeof(ILogEvent), "SaveTo", "logEvidence", typeof(ILogEvidence)),
			InlineData (typeof(ILogEvidence), "Add", "data", typeof(object[])),
		]
		public void Interface_MethodHasOneParameter (Type type, string methodname, string paramname, Type paramtype)
		{
			var args = type.GetMethod (methodname).GetParameters ();
			Assert.Equal (1, args.Length);
			Assert.Equal (paramname, args [0].Name);
			Assert.Equal (paramtype, args [0].ParameterType);
		}
	}
}

