﻿using BrightOpen.LogAway.Evidence;
using System.Collections;
using FluentAssertions;
using Xunit;

namespace Test.LogAway.Evidence
{
    public class StructuredEvidenceTests
    {
        [Fact]
        public void Add_AddsKeyValue()
        {
            var sut = new StructuredEvidence();

            sut.Add("A", "B");

            Assert.Equal("B", sut["A"]);
        }

        [Fact]
        public void Add_AddsSingleValueAsMessage()
        {
            var sut = new StructuredEvidence();

            sut.Add("B");

            Assert.Equal("B", sut["Message"]);
        }

        [Fact]
        public void Add_DoesNotAddEmpty()
        {
            var sut = new StructuredEvidence();

            sut.Add(new object[0]);

            Assert.Equal(0, sut.Count);
        }

        [Fact]
        public void Add_DoesNotAddNull()
        {
            var sut = new StructuredEvidence();

            sut.Add(null as object[]);

            Assert.Equal(0, sut.Count);
        }

        [Fact]
        public void Add_DoesNotAddNullTwice()
        {
            var sut = new StructuredEvidence();

            sut.Add("nullable", null);
            sut.Add("nullable", null);

            Assert.Equal(1, sut.Count);
        }

        [Fact]
        public void Add_AddsTree()
        {
            var sut = new StructuredEvidence();

            sut.Add("A", "B", "C", "D");

            Assert.Equal("D", ((sut["A"] as IDictionary)["B"] as IDictionary)["C"]);
        }

        [Fact]
        public void Add_MergesTrees()
        {
            var sut = new StructuredEvidence();

            sut.Add("A", "Q");
            sut.Add("A", "B", "C", "D");
            sut.Add("A", "x", "y", "z");

            Assert.Equal("D", ((sut["A"] as IDictionary)["B"] as IDictionary)["C"]);
            Assert.Equal("z", ((sut["A"] as IDictionary)["x"] as IDictionary)["y"]);
            Assert.Equal("Q", (sut["A"] as IDictionary)[""]);
        }

        [Fact]
        public void Add_MergesArrays()
        {
            var sut = new StructuredEvidence();

            sut.Add("0", 1);
            sut.Add("0", 2);
            sut.Add("0", 3);

            Assert.IsAssignableFrom<IEnumerable>(sut["0"]);
            (sut["0"] as IEnumerable).Should().BeEquivalentTo(new[] { 1, 2, 3 } as IEnumerable);
        }
    }
}

