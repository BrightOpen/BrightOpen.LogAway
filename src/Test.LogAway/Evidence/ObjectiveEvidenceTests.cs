﻿using BrightOpen.LogAway.Evidence;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using FluentAssertions;
using Xunit;

namespace Test.LogAway.Evidence
{
    public class ObjectiveEvidenceTests
    {
        [Fact]
        public void Add_AddsKeyValue()
        {
            var sut = new ObjectiveEvidence();

            sut.Add("A", "B");

            Assert.Equal("B", sut.Single().Last());
        }

        [Fact]
        public void Add_AddsSingleValue()
        {
            var sut = new ObjectiveEvidence();

            sut.Add("B");

            Assert.Equal("B", sut.Single().Single());
        }

        [Fact]
        public void Add_AddsEmpty()
        {
            var sut = new ObjectiveEvidence();

            sut.Add(new object[0]);

            Assert.Equal(1, sut.Count());
        }

        [Fact]
        public void Add_AddsNull()
        {
            var sut = new ObjectiveEvidence();

            sut.Add(null as object[]);

            Assert.Equal(1, sut.Count());
        }

        [Fact]
        public void Add_AddsNullTwice()
        {
            var sut = new ObjectiveEvidence();

            sut.Add("nullable", null);
            sut.Add("nullable", null);

            Assert.Equal(2, sut.Count());
        }

        [Fact]
        public void Add_AddsComplex()
        {
            var sut = new ObjectiveEvidence();

            sut.Add("A", "B", "C", "D");

            sut.Single().ShouldBeEquivalentTo(new[] { "A", "B", "C", "D" });
        }

        [Fact]
        public void Add_AddsMultiple()
        {
            var sut = new ObjectiveEvidence();

            sut.Add("A", "Q");
            sut.Add("A", "B", "C", "D");
            sut.Add("A", "x", "y", "z");

            (sut as IEnumerable<object[]>)
                .ShouldBeEquivalentTo(
                    new[]
                    {
                        new[] {"A", "Q"},
                        new[] {"A", "B", "C", "D"},
                        new[] {"A", "x", "y", "z"},
                    });
        }

        [Fact]
        public void Add_AddsArrays()
        {
            var sut = new ObjectiveEvidence();

            sut.Add("0", 1);
            sut.Add("0", 2);
            sut.Add("0", 3);

            (sut as IEnumerable<object[]>)
                .ShouldBeEquivalentTo(
                    new[]
                    {
                        new object[] {"0", 1},
                        new object[] {"0", 2},
                        new object[] {"0", 3},
                    });
        }
    }
}

