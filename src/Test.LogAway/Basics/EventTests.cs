﻿using System;
using BrightOpen.LogAway.Basics;
using NSubstitute;
using FluentAssertions;
using Xunit;
using BrightOpen.LogAway;

namespace Test.LogAway.Basics
{
    public class EventTests
    {

        [Fact]
        public void WithEvidence_CreatesNewInstance()
        {
            var sut = new Event();
            var sut2 = sut.WithEvidence("abc");

            Assert.NotEqual(sut, sut2);
        }

        [Fact]
        public void WithEvidence_DoesNotModifyParent()
        {
            var sut = new Event();
            sut.WithEvidence("abc");

            var mockData = Substitute.For<ILogEvidence>();

            sut.SaveTo(mockData);

            mockData.ReceivedCalls().Should().BeEmpty();
        }

        [Fact]
        public void WithEvidence_AddsData()
        {
            var sut = new Event();
            var sut2 = sut.WithEvidence("abc");

            var mockData = Substitute.For<ILogEvidence>();

            sut2.SaveTo(mockData);

            mockData.Received(1).Add("abc");
        }

        [Theory,
            InlineData("", null),
            InlineData("xxx", "yyy"),
        ]
        public void SaveTo_SaveslogEvidence(object key, object value)
        {
            var mockData = Substitute.For<ILogEvidence>();

            var sut = new Event().WithEvidence(key, value);

            sut.SaveTo(mockData);

            mockData.Received(1).Add(key, value);
        }

        [Fact]
        public void SaveTo_CallsNestedEventSaveTo()
        {
            var mockEvent = Substitute.For<ILogEvent>();
            var mockData = Substitute.For<ILogEvidence>();

            var sut = new Event(mockEvent).WithEvidence(String.Empty, null);

            sut.SaveTo(mockData);

            mockEvent.Received(1).SaveTo(mockData);
        }
    }
}

